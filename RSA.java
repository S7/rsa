import java.util.*;
import java.io.*;
import java.nio.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Peter Severin Rasmussen
 */
public class RSA {

    public static void main(String[] args) {
        KeyPair kp_ex = new KeyPair(new PublicKey(1517, 227), new PrivateKey(1517, 203));
        System.out.println(encryptMessage("Dette er en test", kp_ex.pub));
        encryptFile("test.txt", "test.rsa", kp_ex.pub);
        decryptFile("test.rsa", "test.dec", kp_ex.pri);
        //System.out.println(encrypt(423, kp_ex.pub));
        //System.out.println(decrypt(589, kp_ex.pri));

        /*KeyPair kp = generateKeys(1000,2000);
        System.out.println(kp);

        int m = 1000;
        int c = encrypt(m, kp.pub);
        int m2 = decrypt(c, kp.pri);
        System.out.println(m);
        System.out.println(c);
        System.out.println(m2);*/
    }

    // Generate a public key and a private key
    public static KeyPair generateKeys(int min, int max) {
        int p = randomPrime(min, max);
        int q;
        while (true) {
            q = randomPrime(min, max);
            if (q != p) {
                break;
            }
        }
        int N = p * q;
        int f = (p - 1) * (q - 1);
        int e;
        while (true) {
            e = randomBetween(2, f - 1);
            if (gcd(e, f) == 1) {
                break;
            }
        }
        int d = multInverse(e, f);
        PublicKey pub = new PublicKey(N, e);
        PrivateKey pri = new PrivateKey(N, d);
        return new KeyPair(pub, pri);
    }

    // Encrypt an integer using a public key
    public static int encrypt(int m, PublicKey pub) {
        return encrypt(m, pub.e, pub.N);
    }

    public static int encrypt(int m, int e, int N) {
        return modularExponentiation(m, e, N);
    }

    // Decrypt a message using a private key
    public static int decrypt(int m, PrivateKey pri) {
        return encrypt(m, pri.d, pri.N);
    }

    public static int decrypt(int c, int d, int N) {
        return modularExponentiation(c, d, N);
    }

    // String encryption
    public static String encryptMessage(String m, PublicKey pub) {
        StringBuilder sb = new StringBuilder(m);
        char ch;
        for(int i = 0; i < m.length(); i++) {
            ch = (char) encrypt((int) m.charAt(i), pub);
            sb.setCharAt(i, ch); 
        }
        return sb.toString();
    }

    public static String decryptMessage(String m, PrivateKey pri) {
        StringBuilder sb = new StringBuilder(m);
        char ch;
        for(int i = 0; i < m.length(); i++) {
            ch = (char) decrypt((int) m.charAt(i), pri);
            sb.setCharAt(i, ch);
        }
        return sb.toString();
    }
    
    // File handling
    public static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }

    public static String readFile(String path) {
        Charset encoding = Charset.defaultCharset();
        try {
            return readFile(path, encoding);
        } catch(IOException e) {
            System.out.println("IO Exception: " + e);
            return null;
        }
    }

    public static void writeFile(String filename, String content) {
        PrintWriter out = null; 
        try {
            out = new PrintWriter(filename);
            out.write(content);
            out.close();
        } catch (FileNotFoundException e) {
            System.out.println("File '" + filename + "' not found");
        }
    }

    public static void encryptFile(String source, String destination, PublicKey pub) {
        String sourceFile = readFile(source);
        writeFile(destination, encryptMessage(sourceFile, pub));
    }
    
    public static void decryptFile(String source, String destination, PrivateKey pri) {
        String sourceFile = readFile(source);
        writeFile(destination, decryptMessage(sourceFile, pri));
    }

    // Crack a public key to achieve a private key
    // Note: Factoring is hard!
    public static PrivateKey crack(PublicKey pub) {
        return new PrivateKey(pub.N, crack(pub.N, pub.e));
    }

    public static int crack(int N, int e) {
        int p = factorize(N); // This might take a long time for big numbers
        int q = N / p;
        int f = (p - 1) * (q - 1);
        return multInverse(e, f);
    }

    // Finds the first factor of a number
    public static int factorize(int number) {
        if (number % 2 == 0) {
            return 2;
        } else {
            double sqrt = Math.sqrt(number);
            for (int i = 3; i < sqrt; i += 2) {
                if (number % i == 0) {
                    return i;
                }
            }
            return -1;
        }
    }

    // Prime check
    public static boolean isPrime(int number) {
        if (number == 2) {
            return true;
        }
        if (number == 3) {
            return true;
        }
        if (number % 2 == 0) {
            return false;
        }
        if (number % 3 == 0) {
            return false;
        }

        int i = 5;
        int w = 2;
        while (i * i <= number) {
            if (number % i == 0) {
                return false;
            }
            i += w;
            w = 6 - w;
        }

        return true;
    }

    // Generate a random prime between
    public static int randomPrime(int min, int max) {
        for (int tries = 0;; tries++) {
            int cand = randomBetween(min, max);
            if (isPrime(cand)) {
                return cand;
            }
        }
    }

    private static int randomBetween(int min, int max) {
        return ((int) (Math.random() * (max - min))) + min;
    }

    // Perform modular exponentiation
    public static int modularExponentiation(int base, int exp, int mod) {
        if (exp < 0) {
            throw new IllegalArgumentException("Exponent " + exp + " is negative.");
        }
        if (exp == 0) {
            return 1;
        }
        if (exp == 1) {
            return (base % mod);
        }
        if (exp % 2 == 1) {
            return (base * modularExponentiation(base, exp - 1, mod) % mod);
        } else {
            int c = modularExponentiation(base, exp / 2, mod);
            return ((c * c) % mod);
        }
    }

    // Greatest common divisor
    public static int gcd(int a, int b) {
        int r;
        r = a % b;
        while (r > 0) {
            a = b;
            b = r;
            r = a % b;
        }
        return b;
    }

    // Find the multiplicative inverse 
    // Using the extended euclidiean algorithm
    public static int multInverse(int num, int mod) {
        EucRecord rec = extendedEuclidean(num, mod);
        int result = rec.s;
        while (result < 0) {
            result = (result + mod) % mod;
        }
        return result;
    }

    public static EucRecord extendedEuclidean(int a, int b) {

        java.util.List<EucRecord> list = new java.util.ArrayList<EucRecord>();

        list.add(new EucRecord(a, 0, 1, 0));
        list.add(new EucRecord(b, 0, 0, 1));

        int r, q, s, t;
        EucRecord rec, rec1, rec2;
        int i = 2;

        while (true) {
            rec1 = list.get(i - 1);
            rec2 = list.get(i - 2);
            r = rec2.r % rec1.r;
            if (r == 0) {
                break;
            }
            q = rec1.r / r;
            s = rec2.s - rec1.q * rec1.s;
            t = rec2.t - rec1.q * rec1.t;
            rec = new EucRecord(r, q, s, t);
            list.add(rec);
            i++;
        }

        return list.get(i - 1);
    }

    // Small class that holds remainder, quotient, s and t. 
    // Used by the extended euclidean algorithm
    private static class EucRecord {

        public int r, q, s, t;

        public EucRecord(int r, int q, int s, int t) {
            this.r = r;
            this.q = q;
            this.s = s;
            this.t = t;
        }

        @Override
        public String toString() {
            return "R(" + this.r + ", " + this.q + ", " + this.s + ", " + this.t + ")";
        }
    }

    // A neutral key
    private static class Key {

        public final int N;
        public final int n;

        public Key(int N, int n) {
            this.N = N;
            this.n = n;
        }

        @Override
        public String toString() {
            return "(" + this.N + ", " + this.n + ")";
        }
    }

    // A public key with key e
    public static class PublicKey extends Key {

        public final int e;

        public PublicKey(int N, int e) {
            super(N, e);
            this.e = e;
        }
    }

    // A private key with key d
    public static class PrivateKey extends Key {

        public final int d;

        public PrivateKey(int N, int d) {
            super(N, d);
            this.d = d;
        }
    }

    // Pair of public key and private key
    public static class KeyPair {

        public final PublicKey pub;
        public final PrivateKey pri;

        public KeyPair(PublicKey pub, PrivateKey pri) {
            this.pub = pub;
            this.pri = pri;
        }

        public KeyPair(PrivateKey pri, PublicKey pub) {
            this(pub, pri);
        }

        @Override
        public String toString() {
            return "(" + this.pub + ", " + this.pri + ")";
        }
    }

}
